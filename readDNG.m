function [image]=readDNG(filename,whiteBalanceCoeff)
% readDNG open, process and read RAW image as matlab image by matlab internals
% default output with linear response curve (gamma 1.0) and 16 bits per pixel
% filename - filename of file to open
% whiteBalanceCoeff - vector of white balance coefficients, can be empty

addpath('RAW_processing_code');
%suppress tifflib warnings. Working :)
warning('off', 'MATLAB:imagesci:Tiff:libraryWarning');
warning on verbose


% - - - checking parameters - - -
if (exist(filename, 'file')~=2)
    error('file does not exists!');
end
if exist('whiteBalanceCoeff','var') && ~isempty(whiteBalanceCoeff)
    %in example, Kolari white balance = [1 0.6563 0.6614]
    if(length(whiteBalanceCoeff)==4)
        whiteBalanceCoeff=whiteBalanceCoeff(1:3);
    else if(length(whiteBalanceCoeff)~=3)
            error('length of argument whiteBalanceCoeff must be equal to 3 or 4');
        end
    end
end

% - - - Reading file - - -
%DNG file has tags in tiff format, read them
tiffStruct = Tiff(filename);

%read raw image data instead of embedded (or not) miniature
offset = getTag(tiffStruct,'SubIFD');
setSubDirectory(tiffStruct,offset(1));
rawData = read(tiffStruct);
close(tiffStruct);


metaInfo = imfinfo(filename);
imageSub= metaInfo.SubIFDs{1};%tags embedded to dng image

% - - - Linearize - - -
%Digital Negative Specification
%LinearizationTable describes a lookup table that maps stored values into
%linear values. This tag is typically used to increase compression ratios
%by storing the raw data in a non-linear, more visually uniform space with
%fewer total encoding levels

if isfield(imageSub,'LinearizationTable')
    ltab=imageSub.LinearizationTable;
    rawData = ltab(rawData+1);%shift by one, because matlab array indexing starts from one.
end


%Crop to default crop area (active area was bigger than default crop area)
xOrigin = imageSub.ActiveArea(2)+1;
width = imageSub.DefaultCropSize(1);
yOrigin = imageSub.ActiveArea(1)+1;
height = imageSub.DefaultCropSize(2);
rawData =rawData(yOrigin:yOrigin+height-1,xOrigin:xOrigin+width-1);

%sadly, there is no difference between black level in different channels
blackLevel = imageSub.BlackLevel(1);

%luckly, matlab do not rotate values of unsigned operations, when result of
%operation is negative. Simply, result of operation for pixels values less
%than blackLevel is 0
rawData=rawData-blackLevel;

% - - - White Balance - - -
if ~exist('whiteBalanceCoeff','var') || isempty(whiteBalanceCoeff)
    %default value of white balance - camera white balance
    whiteBalanceCoeff=(metaInfo.AsShotNeutral);
end
%if white balance values was provided as less than one values (norm in DNG
%standard, not a norm in user visible values)
if dot(whiteBalanceCoeff,whiteBalanceCoeff) < 3
    whiteBalanceCoeff=whiteBalanceCoeff.^-1;
end

%white balance coefficients should be normalized
[~,index] = min(whiteBalanceCoeff);
whiteBalanceCoeff = whiteBalanceCoeff./whiteBalanceCoeff(index);

%Important! If CFALayout tag is not equal to 1, this code below this point
%will not work as expected! (you should expect mess between channels)
bayerType = 'rggb';
maskWBS = wbmask(height,width,whiteBalanceCoeff,bayerType);

balancedBayerWB = double(rawData) .* maskWBS;
clear rawData;


%demoisaicing by internal matlab function
image= demosaic(uint16(balancedBayerWB),bayerType);
clear balancedBayerWB;
image=double(image); % time to be in floating point arithmetic?

%postfiltering by median filter
image=medianFiltering(image, 3);

%saturation should be the last step in raw processing
saturationLevel = imageSub.WhiteLevel;
%saturationLevel = max(reshape(image,[],1));
image = image*((2^16-1)/saturationLevel);

% - - - Color Correction Matrix from DNG Info - - -
temp = metaInfo.ColorMatrix2;
xyz2cam = reshape(temp,3,3)';

% - - - Color Space Conversion - - -

srgb2xyz = [0.4124564 0.3575761 0.1804375;
    0.2126729 0.7151522 0.0721750;
    0.0193339 0.1191920 0.9503041];

rgb2cam = xyz2cam * srgb2xyz;
rgb2cam = rgb2cam ./ repmat(sum(rgb2cam,2),1,3);
cam2rgb = rgb2cam^-1;

image = apply_cmatrix(double(image),cam2rgb);
image = uint16(image);
