function worker(filenameJPG, filenameRAW, outputPath, outputType, circlePixels, NDVI_map)
Image_JPEG=imread(filenameJPG);
xy = size(Image_JPEG);

% Crop image (standard size only)
if xy(1)==3264 && xy(2)==4928
    Image_JPEG = Image_JPEG(150:3149,1000:3999,:);
end

%Image_JPEG = changeGamma(Image_JPEG, 2.2);

% Calculate NDVI
[NDVI, ~, VIS]=NDVICalc(Image_JPEG);
NDVI(circlePixels==0)=-1;

% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

image= readDNG(filenameRAW);
imageCustomWB= readDNG(filenameRAW,[1 0.6563 0.6614]);%Kolari White Balance
image = image(150:3149,1000:3999,:);
imageCustomWB = imageCustomWB(150:3149,1000:3999,:);

NDVI2_WBS = NDVICalc(image);
NDVI2_WBK = NDVICalc(imageCustomWB);

% Mask the image outside of the circle
NDVI2_WBS(circlePixels==0)=-1;
NDVI2_WBK(circlePixels==0)=-1;

[pathstr,name,~] = fileparts(filenameJPG);

if outputType==1
    % Create a figure showing the photo and NDVI from RAW - set and Kolari white balance
    space = 0.04;
    width=0.43;
    height=0.43;
    height2=0.18;
    left1= 0.02;
    left2= width+left1*2;
    bottom1=space*2+height;
    bottom2=0.03;
    fontsize1=12;
    
    hFig = figure();
    set(hFig, 'Position', [600 0 900 950],'Color',[1 1 1]);
    title(name,'FontWeight','bold');
    
    P1 = subplot(2,3,1); imshow(Image_JPEG);title('JPEG image','FontWeight','bold');
    P2 = subplot(2,3,2); imshow(NDVI2_WBS,[-1 1],'Colormap',NDVI_map);title('NDVI from RAW (DNG), white balance as shot','FontWeight','bold');
    P3 = subplot(2,3,3); imshow(NDVI2_WBK,[-1 1],'Colormap',NDVI_map);title('NDVI from RAW (DNG), Kolari white balance','FontWeight','bold');
    Colorbar_NDVI = colorbar('peer',P3);
    set(Colorbar_NDVI,'YTick',[-1 -0.75 -0.5 -0.25 0 0.25 0.5 0.75 1])
    tix=get(Colorbar_NDVI,'Ytick')';
    set(Colorbar_NDVI,'Yticklabel',num2str(tix,'%.2f'))
    P4 = subplot(2,3,4);hist(NDVI2_WBS(:),100);text(-0.5,900000,'NDVI from RAW, white balance as shot','FontSize',10,'FontWeight','bold','FontName','Arial');
    set(gca,'YLim',[0 10^6]);
    P5 = subplot(2,3,5);hist(NDVI2_WBK(:),100);text(-0.5,900000,'NDVI from RAW, Kolari white balance','FontSize',10,'FontWeight','bold','FontName','Arial');
    set(gca,'YLim',[0 10^6]);
    
    set(P1,'Position',[left1 bottom1 width height]);
    set(P2,'Position',[left1 bottom2 width height]);
    set(P3,'Position',[left2 bottom2 width height]);
    set(P4,'Position',[left2+0.02 bottom1+height2+bottom2*2 width-space height2]);
    set(P5,'Position',[left2+0.02 bottom1 width-space height2]);
    ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
    text(0.5,0.99,name(2:end),'FontWeight','bold','HorizontalAlignment','center','VerticalAlignment', 'top')
    
    %p=getframe(hFig);
    %imwrite(p.cdata,[outputPath, name(1:8),'.tif']);
    print(hFig, [outputPath, name(1:8),'.pdf'], '-dpdf');
   
    %close all;
    
elseif outputType==2
    % Create a figure showing the photo, VIS channel, IR channel and NDVI
    space = 0.04;
    width=0.43;
    height=0.43;
    left1= 0.02;
    left2= width+left1*2;
    bottom1=space*2.5+height;
    bottom2=space;
    fontsize1=12;
    
    hFig = figure();
    set(hFig, 'Position', [600 0 1000 900],'Color',[1 1 1]);
    
    P1 = subplot(2,2,1); imshow(Image_JPEG);title('JPEG image','FontWeight','bold');
    P2 = subplot(2,2,2); subimage(VIS,[0 256]);title('Blue channel (blue light)','FontWeight','bold');
    set(gca,'XTickLabel','');set(gca,'YTickLabel','');
    P3 = subplot(2,2,3); subimage(IR,[0 256]);title('Red channel (Near Infrared)','FontWeight','bold');
    set(gca,'XTickLabel','');set(gca,'YTickLabel','');
    P4 = subplot(2,2,4); imshow(NDVI2_WBS,[-1 1],'Colormap',NDVI_map);title('NDVI from RAW, white balance as shot','FontWeight','bold');
    Colorbar_NDVI = colorbar('peer',P4);
    set(Colorbar_NDVI,'YTick',[-1 -0.75 -0.5 -0.25 0 0.25 0.5 0.75 1])
    tix=get(Colorbar_NDVI,'Ytick')';
    set(Colorbar_NDVI,'Yticklabel',num2str(tix,'%.2f'))
    
    set(P1,'Position',[left1 bottom1 width height]);
    set(P2,'Position',[left2 bottom1 width height]);
    set(P3,'Position',[left1 bottom2 width height]);
    set(P4,'Position',[left2 bottom2 width height]);
    ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
    text(0.5,0.98,name(2:end),'FontWeight','bold','HorizontalAlignment','center','VerticalAlignment', 'top')
    
    %p=getframe(hFig);
    %imwrite(p.cdata,[outputPath, name(1:8),'.tif']);
    %print(hFig, [outputPath, name(1:8),'.pdf'], '-dpdf');
    cdata=print(hFig, [outputPath, name(1:8),'.pdf'], '-dpdf');
    %close all;
    
elseif outputType==3
    % Create a figure with NDVI from JPEG and RAW (white balance as shot)
    space = 0.04;
    width=0.43;
    height=0.83;
    left1= 0.02;
    left2= width+left1*2;
    fontsize1=12;
    
    hFig = figure();
    if(quiet)
        set(hFig, 'visible','off');
    end
    set(hFig2, 'Position', [600 0 1000 500],'Color',[1 1 1]);
    P1 = subplot(1,2,1); imshow(NDVI,[-1 1],'Colormap',NDVI_map);title('NDVI from JPEG','FontWeight','bold');
    P2 = subplot(1,2,2); imshow(NDVI2_WBS,[-1 1],'Colormap',NDVI_map);title('NDVI from RAW, white balance as shot','FontWeight','bold');
    Colorbar_NDVI = colorbar('peer',P2);
    set(Colorbar_NDVI,'YTick',[-1 -0.75 -0.5 -0.25 0 0.25 0.5 0.75 1])
    tix=get(Colorbar_NDVI,'Ytick')';
    set(Colorbar_NDVI,'Yticklabel',num2str(tix,'%.2f'))
    set(P1,'Position',[left1 space width height]);
    set(P2,'Position',[left2 space width height]);
    ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
    text(0.5,0.98,name(2:end),'FontWeight','bold','HorizontalAlignment','center','VerticalAlignment', 'top')
    
    %p=getframe(hFig);
    %imwrite(p.cdata,[outputPath, name(1:8),'.tif']);
    %close all;
    print(hFig, [outputPath, name(1:8),'.pdf'], '-dpdf');
end
end

