function corrected = applyCmatrixFast(im,cmatrix)
%really fast solution to get color correction
if size(im,3)~=3
    error('Apply cmatrix to RGB image only.')
end
xy = size(im);
im=reshape(im,[],3);
im=im*cmatrix';
corrected=reshape(im,xy);
