function [image]=readDNGDcraw(filename, whiteBalanceCoeff, dcrawParam)
% readDNGDcraw  open, process and read RAW image as matlab image
% default output with linear response curve (gamma 1.0) and 16 bits per pixel
% filename - filename of file to open
% whiteBalanceCoeff - vector of white balance coefficients, can be empty
% dcrawParam - custom dcraw parameters as string

if (exist(filename, 'file')~=2)
    error('file does not exists!');
end
if ~exist('whiteBalanceCoeff','var') || isempty(whiteBalanceCoeff)
    %default value of white balance - camera white balance
    whiteBalanceCoeff=' -w ';
else
    %in example, Kolari white balance = [1 0.6563 0.6614]
    if(length(whiteBalanceCoeff)==3)%copy green1 to green2
        whiteBalanceCoeff(4)=whiteBalanceCoeff(2);
    end
    if(length(whiteBalanceCoeff)==4)
        whiteBalanceCoeff=[' -r ' num2str(whiteBalanceCoeff) ' '];
    else
        error('length of argument whiteBalanceCoeff must be equal to 3 or 4');
    end
end
if ~exist('dcrawParam','var') || isempty(dcrawParam)
    %default value of dcraw parameters, meaning below
    % -4 linear 16bit output
    % -T write output as tiff
    % -v verbose on output
    % -w camera white balance
    dcrawParam=' -4 -T -v ';
else
    %parameter string must starts and ends with space character
    if dcrawParam(1)~=' '
        dcrawParam=[' ' dcrawParam ];
    end
    if dcrawParam(end)~=' '
        dcrawParam=[dcrawParam ' '];
    end
end

if isunix==1
    dcrawExec = 'dcraw';
    dirSymbol='/';
else
    dcrawExec = 'RAW_processing_code\dcraw.exe';
    dirSymbol='\';
end
cmd=[dcrawExec, dcrawParam, whiteBalanceCoeff, filename];
system(cmd);
[pathstr,name,~] = fileparts(filename);
image=imread(strcat(pathstr,name,'.tiff'));
