function rgbHistogram(im)
maxValue=max(reshape(im, [], 1));

if isa(im,'uint8')
    %maxValue=2^8-1
    x=0:maxValue;
elseif isa(im, 'uint16')
    %maxValue=2^16-1
   	x=0:maxValue;
else
    %maxValue=1;
    x=0:1/(2^16-1):maxValue;
end
im=reshape(im,[], 3);
red = histc(im(:, 1),x);
green =histc(im(:, 2),x);
blue =histc(im(:, 3),x);
plot(x,red, 'r-', x, green,'g-', x, blue, 'b-');