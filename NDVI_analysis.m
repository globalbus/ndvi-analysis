%% NDVI calculation
% Version beta created by Janka, July 2014
% RAW image processing code from
% http://users.soe.ucsc.edu/~rcsumner/rawguide/ (July 2014)

%% Get the list of images

%clear all
addpath('RAW_processing_code');
if isunix==1
    dirSymbol='/';
    nonDir='\';
else
    dirSymbol='\';
    nonDir='/';
end
% Set your local paths
pictures=input('Which set of pictures? (1, 2 or 3):  ');
if pictures == 1
    %folderpath_JPEG='C:\Users\xkonaj\Documents\_STUDIES\Photos\Fish eye photos\Sigma 4.5 mm\d5100 IR\JPEG1\';
    %folderpath_RAW='C:\Users\xkonaj\Documents\_STUDIES\Photos\Fish eye photos\Sigma 4.5 mm\d5100 IR\DNG1\';
    %outputpath='C:\Users\xkonaj\Documents\_STUDIES\Photos\Fish eye photos\Sigma 4.5 mm\d5100 IR\Output1a\';
    %outputpath2='C:\Users\xkonaj\Documents\_STUDIES\Photos\Fish eye photos\Sigma 4.5 mm\d5100 IR\Output1b\';
    %outputpath3='C:\Users\xkonaj\Documents\_STUDIES\Photos\Fish eye photos\Sigma 4.5 mm\d5100 IR\Output1c\';
    folderpath_JPEG='../zdjecie1/';
    folderpath_RAW='../zdjecie1/';
    outputPath='../Output1/';
elseif pictures==2
    folderpath_JPEG='C:\Users\xkonaj\Documents\_STUDIES\Photos\Fish eye photos\Sigma 4.5 mm\d5100 IR\JPEG2\';
    folderpath_RAW='C:\Users\xkonaj\Documents\_STUDIES\Photos\Fish eye photos\Sigma 4.5 mm\d5100 IR\DNG2\';
    outputPath='C:\Users\xkonaj\Documents\_STUDIES\Photos\Fish eye photos\Sigma 4.5 mm\d5100 IR\Output2\';
elseif pictures==3
    folderpath_JPEG='C:\Users\xkonaj\Documents\_STUDIES\Photos\Fish eye photos\Sigma 4.5 mm\d5100 IR\JPEG3\';
    folderpath_RAW='C:\Users\xkonaj\Documents\_STUDIES\Photos\Fish eye photos\Sigma 4.5 mm\d5100 IR\DNG3\';
    outputPath='C:\Users\xkonaj\Documents\_STUDIES\Photos\Fish eye photos\Sigma 4.5 mm\d5100 IR\Output3\';
else
    disp('Folder does not exist');
    break
end

% Create lists of all images
list_JPEG=dir(fullfile(folderpath_JPEG,'*.JPG'));
list_JPEG=char({list_JPEG.name}');
list_JPEG_str = list_JPEG(:,5:8); list_JPEG_nr = str2num(list_JPEG_str);
list_RAW=dir(fullfile(folderpath_RAW,'*.dng'));list_RAW=char({list_RAW.name}');
list_RAW_str = list_RAW(:,5:8); list_RAW_nr = str2num(list_RAW_str);

% Check
if isequal(list_JPEG_nr,list_RAW_nr) == 0
    disp('Lists unequal')
end

%% Create a custom colormap for NDVI images
m = 256;NDVI_map = zeros(m,3);
tt(1,:) = [0, 0, 0]./256;
tt(2,:) = [256, 0, 0]./256;
tt(3,:) = [256, 256, 0]./256;
tt(4,:) = [0, 200, 0]./256;
tt(5,:) = [0, 100, 0]./256;

%Set where the colours change
b1 = 110;
b2 = 150;
b3 = 175;
b4 = 200;
b5 = 256;

R = linspace(tt(1,1),tt(2,1),b1);G = linspace(tt(1,2),tt(2,2),b1);B = linspace(tt(1,3),tt(2,3),b1);
NDVI_map(1:b1, :) = [R', G', B'];
R = linspace(tt(2,1),tt(3,1),b2-b1+1);G = linspace(tt(2,2),tt(3,2),b2-b1+1);B = linspace(tt(2,3),tt(3,3),b2-b1+1);
NDVI_map(b1:b2, :) = [R', G', B'];
R = linspace(tt(3,1),tt(4,1),b3-b2+1);G = linspace(tt(3,2),tt(4,2),b3-b2+1);B = linspace(tt(3,3),tt(4,3),b3-b2+1);
NDVI_map(b2:b3, :) = [R', G', B'];
R = linspace(tt(4,1),tt(5,1),b4-b3+1);G = linspace(tt(4,2),tt(5,2),b4-b3+1);B = linspace(tt(4,3),tt(5,3),b4-b3+1);
NDVI_map(b3:b4, :) = [R', G', B'];
R = linspace(tt(5,1),tt(5,1),b5-b4+1);G = linspace(tt(5,2),tt(5,2),b5-b4+1);B = linspace(tt(5,3),tt(5,3),b5-b4+1);
NDVI_map(b4:b5, :) = [R', G', B'];
clear b1 b2 b3 b4 b5 m tt R G B

% Mask the image (can be calculated once, then all images have the same size
centerX = 1478;centerY = 1490;radius = 1414; %fully sure about this values?
[columnsInImage rowsInImage] = meshgrid((1:3000) -centerX, (1:3000) - centerY);
circlePixels = rowsInImage.^2 +columnsInImage.^2 <= radius.^2;
clear columnsInImage rowsInImage

%% Load images, calculate NDVI

% Load JPEG image
% Which figure to create?
outputType=input('Choose the figure type: 1, 2 or 3:   ');
switch(outputType)
    case 1
    outputPath=[ outputPath(1:end-1) 'a' dirSymbol];
    case 2
    outputPath=[ outputPath(1:end-1) 'b' dirSymbol];
    case 3
    outputPath=[outputPath(1:end-1) 'c' dirSymbol];
    otherwise
    error('there is no figure');
end
outputPath=strrep(outputPath, nonDir, dirSymbol);
%for being quiet, comment to make figures visible again
set(0,'DefaultFigureVisible','off');
%this loop should be parallelized
for i=1:length(list_JPEG_nr(:,1))
    worker([folderpath_JPEG list_JPEG(i,:)], [folderpath_RAW list_RAW(i,:)], outputPath, outputType, circlePixels, NDVI_map);
end
