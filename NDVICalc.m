function [ndviImage, IR, VIS] = NDVICalc(inputImage)
%NDVI Calculates NDVI image from provided image
IR = im2double(inputImage(:,:,1)); % red channel, IR light
VIS = im2double(inputImage(:,:,3)); % blue channel, blue light
ndviImage = (IR-VIS)./(IR+VIS);
end