function [before,after]=removeVignetting(rawfile,biasfile)
if (exist(rawfile, 'file')~=2)
    error('file does not exists!');
end
if (exist(biasfile, 'file')~=2)
    error('file does not exists!');
end
%read raw file
rawimage = readDNGDcraw(rawfile, [1, 1, 1], '-4 -T');
%read bias image
biasimage = readDNGDcraw(biasfile, [1, 1, 1], '-4 -T');
luminosityCoeff=mean(reshape(rawimage,[],3))./mean(reshape(biasimage,[],3))
imsize = size(biasimage);
%apply gaussian filter
filtered=imgaussfilt3(biasimage);
%convert to double
dimage=im2double(filtered);
redline=mean(dimage(imsize(1)/2-25:imsize(1)/2+25,:,1));
greenline=mean(dimage(imsize(1)/2-25:imsize(1)/2+25,:,2));
blueline=mean(dimage(imsize(1)/2-25:imsize(1)/2+25,:,3));
%plot for check linearity
line=1:imsize(2);
plot(line,redline,'r',line,greenline,'g',line,blueline,'b');

%calculate linear regression of lines
b(1)=(1:imsize(2)/2)'\redline(1:imsize(2)/2)';
b(2)=(1:imsize(2)/2)'\blueline(1:imsize(2)/2)';
b(3)=(1:imsize(2)/2)'\greenline(1:imsize(2)/2)';
disp('regression coeff');
disp(b);

[x,y]=meshgrid([-imsize(2)/2:-1,1:imsize(2)/2],[-imsize(1)/2:-1,1:imsize(1)/2]);
mask = reshape((reshape(repmat(sqrt(x.*x + y.*y),1,3),[],3).*b+1), imsize);
before=im2double(rawimage);
after=mask.*before;

tiff=Tiff('result.tiff', 'w');
tagstruct.ImageLength = size(after,1);
tagstruct.ImageWidth = size(after,2);
tagstruct.Photometric = Tiff.Photometric.RGB;
tagstruct.BitsPerSample = 16;
tagstruct.SamplesPerPixel = 3;
tagstruct.Compression = Tiff.Compression.None;  
tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky; 
tagstruct.Software = 'MATLAB'; 
tiff.setTag(tagstruct);
tiff.write(im2uint16(after));
tiff.close();